//
//  Randomizer.swift
//  Aquarium
//
//  Created by Роман Макеев on 12.02.2020.
//  Copyright © 2020 Роман Макеев. All rights reserved.
//

import Foundation
import UIKit


class Randomizer {
    
    func randomDirection() -> CGPoint{
        let x = CGFloat.random(in: -1...1)
        let y = CGFloat.random(in: -1...1)
        let l = sqrt(x*x + y*y)
        return CGPoint(x: x/l, y: y/l)
    }
    func randomPoint(size : CGSize) -> CGPoint {
        let rndX = CGFloat.random(in:  size.width...(Sizes.width - size.width))
        let rndY = CGFloat.random(in: size.height...(Sizes.heigh-size.height))
        return CGPoint.init(x: rndX, y: rndY)
    }
    
    
    func randomVector(size : CGSize, lastBorder : Int? = nil) -> CGPoint {
        
        var rnd3 : Int
        if lastBorder != nil {
            switch lastBorder {
            case 1:
                rnd3 = Int.random(in: 2...3)
            case 2:
                let borders = [1,3]
                rnd3 = borders.randomElement()!
                
            default:
                rnd3 = Int.random(in: 1...2)
            }
        } else {
            rnd3 = Int.random(in: 1...3)
        }
        var x : CGFloat
        var y : CGFloat
        switch rnd3 {
        case 1:
            x = size.width / 2.0
            y = CGFloat.random(in: (size.height / 2.0)...(Sizes.heigh - (size.height / 2.0)))
        case 2:
            y = Sizes.heigh - (size.height / 2.0)
            x = CGFloat.random(in: (size.width/2.0)...(Sizes.width-(size.width/2.0)))
        default:
            x = Sizes.width - (size.width / 2.0)
            y = CGFloat.random(in: (size.height/2.0)...(Sizes.heigh-(size.height/2.0)))
        }
        return CGPoint.init(x: x, y: y)
    }
}
