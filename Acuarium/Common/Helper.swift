//
//  Helper.swift
//  Aquarium
//
//  Created by Роман Макеев on 11.02.2020.
//  Copyright © 2020 Роман Макеев. All rights reserved.
//

import Foundation
import UIKit





class Helper {
    static func AnimateDuration(pt1 : CGPoint, pt2 : CGPoint, speed : Float) -> TimeInterval{
        let q1 = Float((pt1.x - pt2.x) * (pt1.x - pt2.x))
        let q2 = Float((pt1.y - pt2.y) * (pt1.y - pt2.y))
        let q3 = q1 + q2
        let distance = q3.squareRoot()
        return TimeInterval(distance/speed)
    }
}

