//
//  Models.swift
//  Aquarium
//
//  Created by Роман Макеев on 11.02.2020.
//  Copyright © 2020 Роман Макеев. All rights reserved.
//

import Foundation
import UIKit

class FishMaker {
    
    var view : UIView?
    static var fishId : Int = 0
    private var randomizer = Randomizer()
    let fishSize : [Sizes] = [.tiny, .middle, .huge]
    let fishTypes : [Types] = [.predatory, .herbivore]
    
     func createFish() -> Fish{
        let rndSize = self.fishSize.randomElement()!
        let rndType = self.fishTypes.randomElement()!
        let fish = Fish(type: rndType, size: rndSize, id: FishMaker.fishId)
        FishMaker.fishId += 1
        self.view?.addSubview(fish.sprite)
        let rndPt = self.randomizer.randomPoint(size: rndSize.size)
        fish.sprite.frame = CGRect(x: rndPt.x, y: rndPt.y, width: rndSize.size.width, height: rndSize.size.height)
        self.view?.layoutSubviews()
        fish.create()
        return fish
    }
}
