//
//  Sizes.swift
//  Aquarium
//
//  Created by Роман Макеев on 12.02.2020.
//  Copyright © 2020 Роман Макеев. All rights reserved.
//

import Foundation
import UIKit

enum Sizes {
    case tiny
    case middle
    case huge
    
    static let heigh = UIScreen.main.bounds.size.height
    static let width = UIScreen.main.bounds.size.width
    
    var size : CGSize {
        switch self {
        case .tiny:
            return CGSize(width: 40, height: 40)
        case .middle:
            return CGSize(width: 80, height: 80)
        case .huge:
            return CGSize(width: 120, height: 120)
        }
    }
    var value : Int {
        switch self {
        case .tiny:
            return 1
        case .middle:
            return 2
        case .huge:
            return 3
        }
    }
}
