//
//  Type.swift
//  Aquarium
//
//  Created by Роман Макеев on 11.02.2020.
//  Copyright © 2020 Роман Макеев. All rights reserved.
//

import Foundation


public enum Types {
    case herbivore
    case predatory
}
