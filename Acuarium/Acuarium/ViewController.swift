//
//  ViewController.swift
//  Aquarium
//
//  Created by Роман Макеев on 11.02.2020.
//  Copyright © 2020 Роман Макеев. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private let seaWeedImage = Sprite(image: UIImage(named: "Seaweed_1"))
    private let barrelImage = Sprite(image: UIImage(named: "Barrel_2"))
    private let nameLabel = UILabel()
    private let versionLabel = UILabel()
    
    private let imageView : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage.init(named: "background")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    private  let fishMaker = FishMaker()
    private var fishes : [Fish?] = []
    var count = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.imageView)
        self.imageView.frame = self.view.frame
        self.fishMaker.view = self.view
        self.view.addSubview(self.seaWeedImage)
        self.view.addSubview(self.barrelImage)
        self.createDisplayLink()
        self.start()
        self.setUpTimer()
        self.nameLabel.text = "Aquarium"
        self.versionLabel.text = "Alpha 0.01"
        self.versionLabel.textColor = UIColor.white
        self.nameLabel.textColor = UIColor.white
        self.nameLabel.translatesAutoresizingMaskIntoConstraints = false
        self.versionLabel.translatesAutoresizingMaskIntoConstraints = false
        self.nameLabel.font = UIFont.systemFont(ofSize: 32, weight: .bold)
        self.versionLabel.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        self.view.addSubview(self.nameLabel)
        self.view.addSubview(self.versionLabel)
        self.setUpConstraints()
    }
    func setUpTimer(){
        let timer = Timer(timeInterval: 0.4,
                          target: self,
                          selector: #selector(updateTimer),
                          userInfo: nil,
                          repeats: true)
        RunLoop.current.add(timer, forMode: .common)
        timer.tolerance = 0.1
    }
    func setUpConstraints(){
        let constraints = [
            self.seaWeedImage.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            self.seaWeedImage.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            self.seaWeedImage.widthAnchor.constraint(equalToConstant: 200),
            self.seaWeedImage.heightAnchor.constraint(equalToConstant: 100),
            self.barrelImage.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            self.barrelImage.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            self.barrelImage.heightAnchor.constraint(equalToConstant: 100),
            self.nameLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.nameLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 50),
            self.versionLabel.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10),
            self.versionLabel.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 10)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    func createBubble(center : CGPoint){
        let bubble = Bubble()
        self.view.addSubview(bubble)
        bubble.frame = CGRect(x:center.x, y:center.y, width: 50, height: 50)
        bubble.animashionFinished = {
            bubble.removeFromSuperview()
        }
        bubble.start(center: center)
    }
    @objc func updateTimer(){
        let deaths = self.checkDeaths()
        for dead in deaths {
            let fish = self.fishes.first(where: {$0?.id == dead})
            if fish != nil {
                self.createBubble(center: fish!!.sprite.center)
                
            }
            fish??.sprite.removeFromSuperview()
            self.fishes.removeAll(where: {$0?.id == dead})
            self.createNewFish()
        }
    }
    func start() {
        for _ in 0...9 {
            self.fishes.append(self.fishMaker.createFish())
        }
    }
    func createDisplayLink() {
        let displaylink = CADisplayLink(target: self,
                                        selector: #selector(step))
        
        displaylink.add(to: .current,
                        forMode: .default)
    }
    
    @objc func step(displaylink: CADisplayLink) {
        for fish in self.fishes {
            fish?.move()
        }

        
    }
    func checkDeaths() -> [Int]{
        var deathCount : [Int] = []
        if self.fishes.count > 1 {
        for i in 0...self.fishes.count - 2 {
            for j in (i+1)...self.fishes.count - 1 {
                guard let fish1 = self.fishes[i],
                    let fish2 = self.fishes[j] else {return []}
                let presentedFrame1 = fish1.sprite.frame
                let presentedFrame2 = fish2.sprite.frame
                if (presentedFrame1.intersects((presentedFrame2))) {

                    if fish1.type == .predatory && ((fish1.size.value > fish2.size.value) || ( (fish2.type == .herbivore) && (fish1.size.value + 1 >= fish2.size.value ) )) {
                        if !deathCount.contains(self.fishes[j]!.id){
                            deathCount.append(self.fishes[j]!.id)
                        }
                    } else if fish2.type == .predatory && ((fish2.size.value > fish1.size.value) || ( (fish1.type == .herbivore) && (fish2.size.value + 1 >= fish1.size.value ) )) {
                        if !deathCount.contains(self.fishes[i]!.id){
                            deathCount.append(self.fishes[i]!.id)
                        }
                    }

                }
            }
        }
    }

        return deathCount
    }
    
    
    func createNewFish(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 15) {
            self.fishes.append(self.fishMaker.createFish())
        }
    }

}

