//
//  Sprite.swift
//  Aquarium
//
//  Created by Роман Макеев on 11.02.2020.
//  Copyright © 2020 Роман Макеев. All rights reserved.
//

import Foundation
import UIKit


class Sprite : UIImageView {
    
    
    
    private var img : UIImage?
    private let images : [UIImage?] = [UIImage.init(named: "fish1")?.withHorizontallyFlippedOrientation(), UIImage.init(named: "fish2")?.withHorizontallyFlippedOrientation(), UIImage.init(named: "fish3")?.withHorizontallyFlippedOrientation()]
    
    override init(image : UIImage? = nil){
        super.init(frame: CGRect.zero)
        if image != nil {
            self.translatesAutoresizingMaskIntoConstraints = false
        }
        self.contentMode = .scaleAspectFill
        self.image = image
        self.setUp()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUp(){
    
      
        
    }
    
    func setFish(type : Types) {
        switch type {
        case .herbivore:
   
            self.img = self.images.randomElement()!
            self.image = img
        case .predatory:
            self.img = UIImage.init(named: "p1")?.withHorizontallyFlippedOrientation()
          
            self.image = img
         
        }
       
    }
    private var flipped = false
    func rotate (vector : CGPoint){
        
        var radians = atan2(vector.x, -vector.y)
        radians -= (CGFloat.pi / 2.0)
        if vector.x < 0 {
            radians += CGFloat.pi
            if !self.flipped {
                self.image = self.img?.withHorizontallyFlippedOrientation()
                self.flipped = true
            }
        } else {
            if self.flipped {
                self.image = self.img
                self.flipped = false
            }
        }
        self.transform = CGAffineTransform(rotationAngle: radians)
    }
    
    
}
