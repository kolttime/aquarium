//
//  Bubble.swift
//  Aquarium
//
//  Created by Роман Макеев on 12.02.2020.
//  Copyright © 2020 Роман Макеев. All rights reserved.
//

import Foundation
import UIKit



class Bubble : UIImageView {
    
    
    var animashionFinished : (() -> Void)?
    
    init() {
        super.init(frame: CGRect.zero)
        self.image = UIImage.init(named: "Bubble_2")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func start(center : CGPoint){
        UIView.animate(withDuration: Helper.AnimateDuration(pt1: center, pt2: CGPoint(x: center.x, y: 0), speed: 50), delay: 0, options: .curveLinear, animations: {
            self.center.y = 0
        }) { (finished) in
            if finished {
                self.animashionFinished?()
            }
        }
    }
    
    
    
}
