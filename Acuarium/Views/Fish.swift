//
//  Fish.swift
//  Aquarium
//
//  Created by Роман Макеев on 11.02.2020.
//  Copyright © 2020 Роман Макеев. All rights reserved.
//

import Foundation
import UIKit

class Fish  {
    
    var sprite: Sprite = Sprite()
    
    var size: Sizes
    
    var type: Types
    
    var speed : CGFloat
    
    var id : Int
    
    private var randomVector : CGPoint?
    
    private var randomizer = Randomizer()
    
    private var lastBorder : Int?
    
    
    
    init(type : Types, size : Sizes, id : Int) {
        self.id = id
        self.type = type
        self.size = size
        switch size {
        case .tiny:
           
            self.speed = 3
        case .middle:
            
            self.speed = 2
        case .huge:
            
            self.speed = 1
        }
       
     
        
    }
    
    
    func create() {
        self.sprite.frame.size = self.size.size
        self.sprite.setFish(type: self.type)
        self.randomVector = self.randomizer.randomDirection()
        self.sprite.rotate(vector: self.randomVector!)
    
    }
    func move(){
        guard let vector = self.randomVector else {return}
        if (self.sprite.center.x - self.size.size.width / 2.0) + vector.x * self.speed > 0 && (self.sprite.center.x + self.size.size.width / 2.0) + vector.x * speed < Sizes.width {
            self.sprite.center.x += vector.x * self.speed
        } else {
            self.randomVector = self.randomizer.randomDirection()
            self.sprite.rotate(vector: self.randomVector!)
            return
        }
        if (self.sprite.center.y - self.size.size.height / 2.0) + vector.y * self.speed > 0 && (self.sprite.center.y + self.size.size.height / 2.0) + vector.y < Sizes.heigh {
            self.sprite.center.y += vector.y * self.speed
        } else {
            self.randomVector = self.randomizer.randomDirection()
            self.sprite.rotate(vector: self.randomVector!)
            return
        }
    }
   
    
    
    
    
}
